<?php 
	
	// debug model
	
	class debugModel extends ModelBase {
		
		public $callBack = NULL;

		function _getDebugType($type,$playerID,$villageID){
			
			switch($type){
				case "hero":
					$this->fixHero($playerID);
				break;
				case "crop":
					$this->fixCrop($villageID);
				break;
				
				default:
					//header("Location: debug.php");
				break;
			}
			
		}
		
		function fixHero($playerID){
			
			// lets fetch row of player's hero
			$result = $this->provider->fetchScalar('SELECT hero_troop_id FROM p_players WHERE id=%s', array($playerID));
			
			// Check hero location
			$getHeroLocation = $this->provider->fetchRow('SELECT * FROM p_players WHERE id=%s', array($playerID));
			$hero_village = $getHeroLocation['hero_in_village_id'];
			
			// lets get capital village ID
			$capital = $this->provider->fetchRow('SELECT * FROM p_villages WHERE player_id =%s AND is_capital="1"', array($playerID));
			$capital_id = $capital['id'];
			
			
			// check if player has any troop movements at the moment
			// prevent to debug hero while he's out on a fight. in code it fixes it auto but still...
			$getActions = $this->provider->fetchScalar('SELECT * FROM p_queue WHERE player_id=%s AND village_id != NULL',array($playerID));
			
			if(!$getActions){ // check if there is no actions
				// yes there no actions at the moment
				
				if($result != NULL){ // check if player has a hero trained
				
					if(empty($hero_village)) {
						// hero is bugged.
						$this->callBack = $this->errorTable(1);
						// lets add hero to player's capital
						$this->provider->executeQuery('UPDATE p_players p SET p.hero_in_village_id=\'%s\' WHERE p.id=%s', array( $capital_id, $playerID ) );
					} else {
						$this->callBack = $this->errorTable(2);
					}

				} else { 
					$this->callBack = $this->errorTable(3);
				}
				
			} else { // else return false and throw error
				$this->callBack = $this->errorTable(4);
			}
		
		}
		
		function fixCrop($villageID){
			
		}
		
		function errorTable($id){
			$errorTable = array(
				1=>"Hero fixed, and hes in your capital",
				2=>"Hero already in capital !",
				3=>"You don't have a hero !",
				4=>"Please finish all troops movements before debug !",
			);
			
			return $errorTable[$id];
		}
	
	}